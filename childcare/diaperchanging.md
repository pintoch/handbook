# :baby_symbol: Diaper changing

We use reusable cloth diapers and have developed quite some techniques for folding the cloth to make good diapers which are appropriate for different age levels and situations.

## General tips for handling the 1-year-old

- It makes a lot of sense to do this job with 2 people!
  - One person can undress and clean the child.
  - The other person can prepare the diaper and put it on the clean child.
  - Then the first person can use that time to clean up the dirty diaper.
- If you do it alone, you probably want to have the diaper prepared _before_ you get the child.
  - Otherwise the child roams free during the time you fold and throws things into the toilet and such...^^'
- If you do it alone it also makes sense to deliver the freshly changed child somewhere else before you take care of the dirty cloths.
  - Otherwise the child will find its own poop fascinating and will desperately try to touch it...^^' 

## Removing a full diaper

There are different approaches depending on the mobility and size of the child, but the general steps are as follows:

1. Remove the legwear.
1. Open the underwear (usually a body with press buttons between the legs).
1. Close the body again above the shoulder to prevent it from getting wet in case the child spontaneously pees.

**If the child lies on their back**
1. Open the diaper and investigate its contents.
1. In case of poop you can use the corners of the used diaper to roughly clean the child.
1. Put the full diaper to the side.
1. Use additional wet cloths to clean the child properly.

**If the child can already stand**
1. Place them in the sink.
1. With a hand below, open the diaper and put it to the side.
1. Open the tab and clean the child with warm water and your hand.
1. Wash your hands.
1. Dry them and the child with a towel.

## Preparing a 4-layer diaper for a 1-year-old

At one year of age the bladder already has a significant size. That is why we use these three layers inside the waterproof diaper pants:

- one holding layer
- one poop layer
- one soaking layer

The idea behind is that no urine will spill even if the child pees a lot, and that possibly poop only touches the specific poop layer.

Here's the folding technique:

### Holding layer

![](/images/diaper_01.jpg)<br>
_Take one of the big 'Mull' cloths that soak very well._

![](/images/diaper_02.jpg)<br>
_Fold it almost in half._

![](/images/diaper_03.jpg)<br>
_Fold in the edges of the lower half._

![](/images/diaper_04.jpg)<br>
_Grab the upper left corner that emerged and fold it down to the bottom._

![](/images/diaper_05.jpg)<br>
_Do the same thing with the other side._

These steps are to make sure that most of the soaking cloth is where it is needed: In front of the genitals.

![](/images/diaper_06.jpg)<br>
_Fold up the lower left corner._

![](/images/diaper_07.jpg)<br>
_Do the same on the right side._

Now you have a nice triangle that will fit nicely around the child's butt. If any of the before-mentioned steps confuse you it's probably okay. The most important thing is to have a triangular shape for this layer, the holding layer.

### Poop layer

![](/images/diaper_08.jpg)<br>
_Take one of the stable tea towels._

![](/images/diaper_09.jpg)<br>
_From the long side fold it almost in half._

![](/images/diaper_10.jpg)<br>
_Halve it again so that the width is enough to hold the droppings of the child while not unnecessarily impeding its movement._

![](/images/diaper_11.jpg)<br>
_Turn it around, place it in the middle of the holding layer and kinda align the top edges._

![](/images/diaper_12.jpg)<br>
_Fold in the bottom part once._

![](/images/diaper_13.jpg)<br>
_Fold in the bottom part another time so that the length of the poop layer fits the length of the holding layer._

![](/images/diaper_14.jpg)<br>
_Turn it around to have a nice and even surface from which the poop will easily separate after use._

Now you have the holding layer with the poop layer on top. The poop layer of course also helps with soaking, that's why we fold in the bottom part twice: The more cloth in front of the child's pee source the better.
For younger children the diaper would now already be finished, but since we want to take care of a 1-year-old, there's still not enough soaking material.

### Soaking layer

![](/images/diaper_15.jpg)<br>
_Take another one of the 'Mull' cloths, preferably a smaller one._

Or actually, take whatever you want that soaks well. It could also be a tiny towel or a piece of an old t-shirt or whatever you have lying around. It's just important to fold it in a way that it still fits into the diaper pants, no need to have a systematic approach here. But if you want one you can follow the instructions still... :wink:

![](/images/diaper_16.jpg)<br>
_Halve it from the long side._

![](/images/diaper_17.jpg)<br>
_Halve it again._

![](/images/diaper_18.jpg)<br>
_Halve it from the other side._

![](/images/diaper_19.jpg)<br>
_And again._

![](/images/diaper_20.jpg)<br>
_Place it between the poop layer and the holding layer and make sure that no parts of it stick out - we don't want the soaking layer to have poop contact!_

To make poop contact of the soaking layer even less likely you can also put it below the holding layer in the diaper pants, like in this picture:

![](/images/diaper_23.jpg)<br>

### Assembling the layers

![](/images/diaper_21.jpg)<br>
_Get a diaper panty, open it up and fold in the sides of the holding layer of the prefolded cloths to make transferring them easier._

![](/images/diaper_22.jpg)<br>
_Place the prefolded diaper inside the diaper panty and extend the sides again._

### Putting the diaper on the child

This part can be tricky so make sure you're not alone when doing it for the first time. 1-year-olds are very mobile so the child might very well just get up and move away if you're not quick and determined enough. Distraction is key here: Give them something to play with already before putting them on their back and/or have the other person interact with them to make it more interesting for the child to stay in place.

Here's the steps you need to take (pictures might follow later):

- You want to place the butt of the child in a way that it is in the middle of the poop layer horizontally and have the upper end aligned with how it should sit on the child's back in the end.
- Fold up the front part first.
- Then take a side and fold it up following the leg shape of the child to have a better fit there, too.
- Direct the long side part towards the front.
- Take the other side and do the same.
- Make sure the soaking layer lies neatly in the diaper pants.
- Quickly close the diaper pants around the whole cloth construction.
- Push in all cloth that you can still see.
- Make sure the diaper doesn't sit too tight, to not cause the child discomfort.
- Make sure the diaper doesn't sit too lose , to not have leakages happen.

That's it. Afterwards you just need to dress the child again and you're done.

## Handling the dirty material

(tbw)
