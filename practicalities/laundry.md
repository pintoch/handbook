# :dress: Laundry

Everything concerning laundry - be it dirty or clean.

### Communal clothing

Since everything in Kanthaus is communal by default, this is the main section.

#### Dirty laundry sorting

Dirty laundry can be put into the laundry containers on each floor (K20-X-1b) or directly sorted into the fitting sections in K20-0-1b. Matthias takes laundry very seriously, so be sure to sort it right or just put it in an unsorted container.

**Operating the washing machine** touches the status of the weather. Please talk to Matthias, Tilmann or Bodhi if you want to do laundry. Also, if the weather forecast foresees rain, it is better not to wash clothes.

#### Wet laundry

Wet laundry is to be put up to dry outside. There are clothes pegs directly on the washing line. Make sure to adequately fix the laundry to the washing line, so that even stronger wind will not make it fall down and get dirty again.

Please leave the last two lines on the left unused if possible, as those are reserved for private laundry.

#### Dry laundry

Take a clean green box from the [washing room](/rooms/washingroom.md) and take down dry laundry. Make sure to leave the clothes pegs on the washing line and to not take down private laundry that is not your own (last two lines on the right). If you don't want to sort the laundry into the [communal closet](/rooms/communalcloset.md), just put the whole box to the floor of that room.

### Private clothing

If you stay only for a short time or have clothes which you don't want to share, you should not put them in the laundry baskets - chances are high that you won't find you stuff again in a reasonable amount of time. You can just approach Matthias, Bodhi or Tilmann and ask for a private run of the washing machine. Please make sure that you have enough things to wash, so that the machine is full, or add some Kanthaus pieces.

For drying your private clothing that should not end up in the [communal closet](/rooms/communalcloset.md) we have a designated space on the washing line: The last two lines on the left.
