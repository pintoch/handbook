# :coffee: Hot Drinks

### Boiling water
- Making tea for others is appreciated :) Especially in the morning, as the thermoses will keep it hot for hours.
- It's the privilege of the person preparing the hot drinks to choose which kind will be made.
- Please don't boil more water than you need! It's OK to measure water, as long as only pure water goes in the kettle.

### Brewing tea (Janina)
- Please economize tea! The used tea in the used dishes shelf can be used again.
- Please don't put loose tea directly into the teapots: it generally makes tea taste worse and drinking inconvenient. We have various tea sieves you use.
- Please label what's in the thermos. There is a marker pen attached to the wall in the snack kitchen and write directly on the thermos the kind of tea inside.

## Brewing tea 
### Black tea
- 1st flush: 3 minutes maximum if you want to reuse the teabag
- 2nd flush: 5-8 minutes maximum, otherwise the tea will taste bitter
- 3rd flush: not recommended for the ratio 1 teabag per 1 liter of water
### Green tea
- 1st flush: 2 minutes
- 2nd flush: 5 minutes
- 3rd flush: 10 minutes
### Fruit infusion
- 1st flush: 5 minutes
- 2nd flush: 8 minutes
- 3rd flush: not recommended for the ratio 1 teabag per 1 liter of water
- feel free to add some sugar or syrup
### Herbal tea
- You can simply leave the teabag in if you don't plan on reusing it
- If you want to reuse it however go like this:
  - 1st flush: 5 minutes
  - 2nd flush: 8 minutes
  - 3rd flush: not recommended for the ratio 1 teabag per 1 liter of water
