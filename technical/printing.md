# :fax: Printing and Scanning
_Last reviewed 2020-11-06 by Matthias_

## Canon imageRunner Advance C2025i
* Location: K20-1-4 Office
* Type: Laser
* Color: CMYK
* Paper size: A4 (Tray 2), A3 (Tray 1), Custom (Manual Feed)
* Duplex: Yes
* Scanning: Duplex ADF, to network share or USB

### Without installation
You should not need to install anything - just open the printing dialog in the software you would like to print from and chose the `CanonC2025PXL` printer.

This printer should be autodetected in all networks except `kanthaus-gast` from which printing is not possible.
Your system needs to be configured to support [MDNS](https://en.wikipedia.org/wiki/Multicast_DNS) (e.g. install & start the `avahi-daemon` on linux, which should be default for most).

Using this, you print using the printer installed on the kanthaus server. While this should normally work, you might consider installing the printer manually on your system to avoid any potential problems and have a more direct relationship to the printer.

### Printer settings
By default, it should do duplex printing in greyscale mode on A4 paper. Feel free to change the settings:
* Page setup: for duplex settings and paper size or manual paper tray
* Options: You might want to adjust page scaling when you chose a different paper size
* Advanced: Enable color print via `Colour Mode` or play with the quality settings if you are not happy with the result when printing images/graphics.

### Manual installation
Install the `canon-cque` package from AUR (Archlinux/Manjaro) or download the DEB or RPM or other appropriate driver from [Canon](https://www.canon-europe.com/support/products/imagerunner/imagerunner-advance-4025i.html?type=drivers&language=en&os=linux%20(64-bit)) if you use another linux system or windows. Canon bundles all imageRunner drivers in the same package, so you can use a newer model to get drivers if none are offered for the C2025i.

Then, add the printer using the tools provided by your system, e.g. the cups webinterface, reachable at [](http://localhost:631). Under `Administration` click `Add printer`. You might be asked for your user credentials of the user you log into your system.
Do not use the auto detected printer on kanthaus-server but add one via `AppSocket`. On the next screen, provide `socket://192.168.4.153:9100` as the connection.

Provide a name of your choice and feel free to leave description and location empty.

Select `Canon` as a brand and `Canon iR-ADV C2025i PXL` as the model.

You might want to set default options for using A4 paper and greyscale print.

### Scan to USB
Insert a USB stick into the printer.
Press the power button on top if it is not turned on, then press the Scan & Save button on the top of the screen and select the USB storage medium.

You might want to set scanning options about filetype, resolution/quality, color mode and duplex. Take a few minutes to understand all the options (and document them here) :-)

### Scan to Network
After turning the printer on, press the Scan & Save button on the top of the screen and select the KH-Server location.

See section above for scanning options.

After scanning, you will find your files in the [Scan](smb://kanthaus-server/scan) share on kanthaus-server. Use the file manager of your system and browse for windows/samba/network shares or directly enter the address: `smb://kanthaus-server/scan`. If you are asked for any credentials, you can login anonymously.

Please remove your files after scanning. Also note that they are accessible to anyone without authentication from all networks except `kanthaus-gast`.
