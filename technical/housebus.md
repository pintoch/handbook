# :zap: House Bus System

> ##### Outdated
> - most of the house bus network got changed / new cables added

The house bus system is a project initiated by Matthias with the idea to collect data, access sensors and use actors in the house.
This ranges from measuring the temperature over managing the grey water system up to operating the door bell.

See the [Hardware/Firmware Github repository](https://github.com/NerdyProjects/HouseBusNode) and the [Logging helper Github repository](https://github.com/yunity/uavcan-influxdb-writer) for further details.

### Topology

The house bus is operated via a 4-wire connection that is passed from one node to another.
It is physically a [CAN Bus](https://en.wikipedia.org/wiki/CAN_bus) using 12..20 V supply voltage and the [UAVCAN protocol](https://en.wikipedia.org/wiki/UAVCAN).
It is operated at 125 kBps and should work with a total bus length of ~200m where deviations from the ideal linear bus are allowed up to 30 m stubs.

#### CAN Termination

The CAN bus needs to be terminated at both ends.
Currently, the termination resistors are in K20-2# (Greywatertank) as well as K20-B-2 (Rainwater tanks).

### Physical access

Bus access is possible everywhere where the cable is.
There should be a list of nodes where each physical split point / access point should be a top level list point and has all connections to other nodes as sublists.
A sublist entry should mention all rooms the cable passes through.

* Node K20-2#
  * to K20-1 hallway via K20-1# and K20-1-1
* K20-1 hallway
  * to K20-2# (via see above)
  * to K20-0 hallway
* K20-0 hallway
  * to K20-1 hallway
  * to K20-0-2
  * to K20-B-4 (electricity main box room) via K20-B-3
* K20-0-2
  * to K20-0 hallway
* K20-B-4
  * to K20-B-2 (rainwatertanks)
  * to K20-B-6 (heating room)
* K20-B-2
  * to K20-B-4
* K20-B-6
  * to K20-B-4


