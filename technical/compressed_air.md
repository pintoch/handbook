# :zap: Compressed air system

### Outlets

* K22-0-4 workshop (next to door)
* Garden (next to K22 door)
* K22 basement (used for bicycle repair station)

### Compressor

The compressor is located in K20-B-1 water storage room. It is a Kaeser Classic 210/50, having a storage volume of 50l and delivers around 100 liters per minute constantly at 6 bars.

A pressure regulator, filter and water separation unit is attached directly to the compressor.

The pressure is set to approximately 7 bars. There is a separate pressure regulator for the bicycle repair station (an additional handbook page for that would be useful).

The manual is available in kanthaus nextcloud: public/topics/workshops and tools

### Maintenance

* If there is a leakage, turn off or unplug the compressor in K20 basement.
* Water needs to be drained from the compressor every few operating hours: There is a small valve unterneath it. Hold a cloth in front of it, open it quite a bit for a few seconds until no water/oil is coming out anymore.
* Compressor oil level should be checked every few months. On the side opposite to the air outlet there is a circle indicating min/max oil level. As of 2020, we didn't refill oil yet and it is still slightly above maximum (since we got it).
* Compressor is leaking tiny amounts of oil out of oil drain screw.
* The whole compressed air system does not have any valves as they would likely induce more complexity and possible leakages into the system. **Do not disconnect the hose from the compressor!** Or if you want to, do it only with hearing protection and beware of the air coming out of the tube you disconnect. Alternatively, use the consenation drain unterneath the compressor to empty it from air first.
