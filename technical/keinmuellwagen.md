# :minibus: KeinMüllWagen

We have a car! A van even! And we use it to save resources... :) /br
so far the user informations are only in german:

/br
/br


Achtung: **Flüssiggas (LPG)** oder **Benzingetriebenes** Auto. /br
Flüssiggastankstutzen hinten rechts neben der Anhängerkupplung, ACME Tankadapter über dem Radio.
Tankanzeige (neben Tacho) gilt nur für Benzin.
Gassteuerung: Rechts neben Zündschloss. Rote LED: Gas-Reserve (fängt an zu blinken bei ~150 Restkilometer, wenn dauerhaft leuchtet, ~50 Restkilometer). Wenn Motor bergrunter/beim Bremsen ruckelt/ausgeht, ist Gas alle!

**Starten (schnell, Benzin):** /br
Kippschalter nach oben (Benzin)
Motor starten, nudelt etwa 5 Sekunden, läuft dann

**Starten (Gas, Motor ist noch frisch warm):** /br
Zündung an, Kippschalter nach unten (Gas).
Motor starten (max. 5 Sekunden nudeln), Noch 2 Sekunden nachnudeln lassen während Motor schon läuft. (Läuft erst auf Restgas im Schlauch, bis neues Gas kommt vergeht 1 Sekunde).
Wenn grüne Gas-LED an Steuerung nicht angeht (nur blinkt), ist Motor zu kalt! Gassteuergerät benötigt Mindest-Drehzahl zum Einschalten.

**Starten (Gas, Motor kalt, „Notstart“):** /br
Zündung an, Kippschalter nach unten (Gas)
Gassteuerknopf 1x drücken, Gas-LED geht aus.
Gassteuerknopf 15 Sekunden gedrückt halten. Es macht „Klack“ gefolgt von leisem „Zisch“.
Nach 2-3 Sekunden Motor starten. Gas geht nach 5 Sekunden ohne Starten automatisch wieder aus.

**Umschalten auf Benzin:** /br
Im Stand Motor hochdrehen (Gaspedal etwas drücken) oder während der Fahrt eingekuppelt sein
Kippschalter nach oben (Benzin)

**Umschalten auf Gas während der Fahrt:** /br
Kippschalter auf Mitte (kein Kraftstoff)
Nach 5-30 Sekunden, je nach Gas, ruckelt Motor/geht aus (Verkehrssituation sollte vorhersehbar sein)
Kippschalter nach unten (Gas)
(Sofern grüne Gas-LED nicht an, Gassteuerknopf 1x drücken)

**Umschaltfehler: Benzin auf Gas ohne Mittelstellung** /br
Motor läuft viel zu Fett, hat keine Leistung/geht aus.
Auf Mitte zurückstellen und warten, bis ruckelt (5-30 Sekunden), dann auf Gas umstellen

**Motor startet nicht** /br
- Umschalter auf Gas: Siehe „**Notstart**“, alternativ auf Benzin probieren.
-  Abgesoffen:
    - Kippschalter nach oben, Benzin
    - 5-7 Sekunden nudeln lassen.
    - Falls er nicht anspringt, 5 Sekunden Pause, Gaspedal halb treten, Anlasser betätigen, dabei Gaspedal voll durchtreten. Nochmal 5 Sekunden nudeln lassen
- Falls er nicht anspringt: Kippschalter auf Mitte. 10 Sekunden nudeln lassen und nochmal von vorn.
- Gas geht nicht: Steuergerät benötigt Mindestdrehzahl, Startversuch wird durch leuchten (statt blinken) der Gas-LED angezeigt.
- 2x Sicherungen Gassteuergerät/Benzinabschalter im Motorraum neben/hinter Batterie.
