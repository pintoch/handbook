# :exclamation: MCM

A structure to decide what to work on in the house for the coming year.

### Scope

After two years in Kanthaus we found that 3 months are too little time to plan things like fixing the roof. We also realized that summer is different to other seasons because people are everywhere  but home in that time. That's why we decided to define the Kanthaus work year from September to May and plan this period in one go.

### More
tbc
