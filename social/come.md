# :white_check_mark: Coordination Meeting

The weekly meeting to coordinate everyone's actions and keep up-to-date with what's going on in/with the house. Colloquially called 'CoMe'.

### Time and place

The default is **Monday 10 am** in the [dining room](/rooms/diningroom.md). The meeting lasts until 11 am maximum.

### Scope and importance

This is the most important meeting at Kanthaus. It takes place every week, we take notes which we [publish on our website](https://kanthaus.online/governance/minutes) and we expect people to take part if they want to know what's going on.

### Progression

A facilitator moderates the meeting, reads out the relevant informational sections and guides through the discussion points. Topics are collected before-hand in the [CoMe pad](https://codi.kanthaus.online/come). This document is then used as base for the meeting by the facilitator.
