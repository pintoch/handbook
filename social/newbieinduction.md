# :beginner: Newcomer Induction

Kanthaus is a complicated place with many rules and potentially unusual habits. That's why we came up with some ways to make it easier for new people to understand what matters here.

### House tour

Every person that wants to stay for some days should be given an introductory house tour in which all the rooms are visited and roughly explained. The new person should be made aware of the ubiquitous signs and of the fact that they convey important information which should be followed. The tour should also include some of the more meta points of how living in Kanthaus generally works.

### Tour document

To achieve a more standardized way of relaying (and not forgetting) important information about living in Kanthaus, Doug created an accompanying [visiTour document](https://gitlab.com/kanthaus/kanthaus-public/blob/master/visiTour.md).

### Handbook

For more detailed descriptions and explanations every interested person may refer to this handbook.
