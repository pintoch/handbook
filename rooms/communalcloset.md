# :womans_clothes: Communal Closet (or K20-2-4)

The communal closet is a room for shared and personal clothes and belongings.

### Facilities (Needs to be updated!)
- The **main shared wardrobe** on the left contains most clothes.
  - The main shared wardrobe consist of three individual wardrobes, which contain from left to right–
    - T-shirts, tops, long sleeves, thin and special jumpers
    - Warm jumpers, zipper jackets, sleep clothes, tights, leggings
    - All kinds of pants, dresses, skirts
  - The clothes in each wardrobe are sorted from top to bottom and from left to right following a 'bigger to smaller' pattern.
- On top of the main shared wardrobe there are boxes for **long-term storage of personal belongings**.
  - Items stored here won't be touched without permission and/or a very good reason.
- The **secondary shared wardrobe** on the right, close to the window, contains more clothes and bedclothes.
  - The open part contains from right to left–
    - Panties of different sizes and materials in the bags nailed to the side
    - Bras in the bag behind the panty bags
    - Boxers in the crate on the bottom
    - Jumpers on the hangers
    - A bag filled with bags next to the jumpers
    - Some fancy jackets
    - Hankies, wrist- and legwarmers in the top department
  - The closed part next to the window contains from top to bottom–
    - Pillow cases (80x80cm on the left and other sizes on the right)
    - Blanket covers in standard sizes (smaller ones on the left, bigger ones on the right)
    - Blanket covers in comfort sizes (roughly 1,50 m x 2,20 m)
    - Various textiles (tablecloths, decorative cloths, sheets without rubber bands)
- The **tertiary shared wardrobe** between the door to the hall way and the one to the [communal sleeping room](communalsleeping.md).
  - The three drawers hold socks sorted by size and form.
  - Behind the left door you'll find bathrobes and westernized kimonos.
- On top of the secondary shared wardrobe there is the **long-term backpack storage** area.
  - Empty and/or unused backpacks can be put here.
  - If you feel attached to your backpack make sure to put your name on it.
- In the middle of the room there are **mid-term individual storage facilities**.
  - For personalized items of frequent use.
  - Compartments can be claimed by putting a named label on them.
  - Preferably not more than 1-2 compartments should be claimed by one person.
  - When leaving for longer than a few days these storage facilities should be emptied, so that they can be used by other people. Personalized stuff can be transferred to a box and put on top of the main shared wardrobe.
- Right of the door to the hallway there is the **night storage shelf**.
  - It has four compartments with two hooks.
  - The compartments here are personalized if a name tag is put onto them.
  - They are meant for people to put their day-clothes before going to bed in the [communal sleeping room](communalsleeping.md).

### Room availability

This room should always be available.

### Specials

This is the room to put clean laundry. If you don't want to sort it in, just put the box in front of the individual storage facilities.  
This is also a room where people change clothes and may run around (almost) naked. No need to freak out about this... ;)
