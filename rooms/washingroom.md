# :cyclone: Washing Room (or K20-0-1)

A room for sorting and cleaning clothes and food. It also has a toilet.

### Facilities
* A customized washing machine
* The place to sort dirty laundry in useful categories
* A sink and a mirror
* Clean (green) boxes and clothes baskets
* Semi-clean plastic bags for dumpster-diving
* An unconnected, elevated bathtub to clean dumpster-dived food and other things
* Buckets for collecting grey water to flush toilets
* A bucket toilet

### Laundry
We have a pretty elaborate idea of how laundry should be done. Please refer to the [laundry page](/practicalities/laundry.md) for further details.

### Food
You may find saved food in here which needs cleaning and sorting. Saving food is one of the main tasks in our house. [Here's the guide](/practicalities/savingfood.md) to do it properly.
